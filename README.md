# Questões teóricas

### 1 - O que é Git?
Criado por Linus Torvalds, Git é uma ferramenta de versionamento de código. Esse tipo de ferramenta permite um monitoramento e controle do código que facilitam a vida do dev. Além, é claro, de facilitar o compartilhamento de código entre devs.

### 2 - O que é a staging area?
É onde as alterações feitas são armazenadas antes de ser realizado um commit. Funciona como uma sala de espera num consultório médico onde o código alterado é o paciente e o commit é a consulta que logo vai ser feita.

### 3 - O que é o working directory?
O diretório onde se encontra os arquivos que estão sendo trabalhados pelo dev naquele momento.

### 4 - O que é um commit?
Uma analogia é a de dar um “save game”. O commit vai persistir as alterações realizadas no repositório local, do mesmo jeito que um save game persiste seu progresso em algum jogo.

### 5 - O que é uma branch?
Branch se trata de uma ramificação. Serve para que consigamos particionar nosso projeto em “galhos” que poderão ser trabalhados de maneira independente da origem. Muito útil quando se está implementando uma funcionalidade que pode vir a ter muitas consequências sobre o projeto ou quando estamos trabalhando em equipes. Assim, uma pessoa responsável por certa funcionalidade pode trabalhar nela sem precisar alterar o projeto como um todo.

### 6 - O que é o head no Git?
Um ponteiro que vai apontar para uma branch ou commit.
### 7 - O que é um merge?
É quando juntamos dois códigos que se tornam diferentes. Um caso possível é o de ter feito commit num arquivo e no futuro tê-lo alterado. Para não haver conflito entre essas alterações, fazemos o merge, verificando se essas mudanças realmente podem ser feitas no tal arquivo.

### 8 - Explique os 4 estados de um arquivo no Git.
**Untracked** - Arquivos que não estavam no último commit (arquivos novos);
**Unmodified** - Não houve nenhuma alteração nesse arquivo desde o último commit;
**Modified** - O arquivo foi alterado desde o último commit;
**Staged** - O arquivo se encontra à espera de um commit.

### 9 - Explique o comando git init.
Inicia o git em um projeto. Ele adiciona a pasta .git e permite que os demais comandos sejam executados.
### 10 - Explique o comando git add.
Esse comando adiciona um ou mais arquivos na staging area.

### 11 - Explique o comando git status.
Ele retorna o estado dos arquivos do working directory.

### 12 - Explique o comando git commit.
Esse comando é responsável por fazer as alterações ficarem persistentes no repositório local.

### 13 - Explique o comando git log.
Ele retorna os commits realizados no working directory.

### 14 - Explique o comando git checkout -b.
Cria uma nova branch e altera para ela após sua criação.

### 15 - Explique o comando git reset e suas três opções.
Ele serve para desfazer alguma alteração realizada no git.
**Soft** - Move o ponteiro HEAD para o commit indicado mantendo o staging e o working directory inalterados;
**Mixed** - O ponteiro HEAD vai para o commit indicado e ao mesmo tempo altera o staging, deixando apenas o working directory inalterado;
**Hard** - O ponteiro HEAD vai para o commit indicado e além disso, tanto o staging quanto o working directory vão para esse estado. Tudo o que foi feito nesses dois antes do commit será perdido.

### 16 - Explique o comando git revert.
Cria um novo commit com o estado de um commit anterior.

### 17 - Explique o comando git clone.
Esse comando permite clonar um repositório caso ele seja público ou seja seu caso for privado.

### 18 - Explique o comando git push.
Serve para enviarmos as alterações do nosso repositório local para um repositório remoto.

### 19 - Explique o comando git pull.
Serve para pegarmos as alterações de um repositório remoto e passarmos para o nosso repositório local.

### 20 - Como ignorar o versionamento de arquivos no Git?
Basta criar um arquivo .gitignore na raiz do working directory e nele adicionarmos os arquivos e diretórios a serem ignorados.

### 21 - No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.
A master/main vai servir como a branch de produção, onde o código estará localizado após passar pela branch de staging, que por sua vez serve para testar os códigos adicionados na branch de develop, que é onde o código é propriamente desenvolvido.

# Questões práticas

### 1 - A essa altura, você já deve ter criado a sua conta do GitLab, não é? Crie um repositório público na sua conta, que vai se chamar Atividade Prática e por fim sincronize esse repositório em sua máquina local.

### 2 - Dentro do seu reposotorio, crie um arquivo chamado README.md e leia o artigo como fazer um readme.md bonitão e deixe esse README.md abaixo bem bonitão: README.md onde o trainne irá continuamente responder as perguntas em formas de commit. 

Inserção de código, exemplo de commit de feature. 

### 3 - Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código: 
`node calculadora.js a b`

```js
const args = process.argv.slice(2);

console.log(parseInt(args[0]) + parseInt(args[1]));
```

Descubra o que esse código faz através de pesquisas na internet, também
descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:

**RESPOSTA:** 
Primeiro, a const args recebe os dois primeiros argumentos passados na execução do arquivo e os salva na forma de array. Após isso, estamos mostrando através do console.log a soma dos dois args, pegando-os da posição 0 e da posição 1 e convertendo um valor que é passado como string para um inteiro.

### 4 - Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.	

**RESPOSTA:** No lugar de usar git add ., podemos simplesmente usar git add <filename>.

* Que tipo de commit esse código deve ter de acordo ao conventional commit.
* Que tipo de commit o seu README.md deve contar de acordo ao conventional commit. 
* Por fim, faça um push desse commit.
  
O `calculadora.js` é um novo arquivo com uma nova funcionalidade. Logo, ele deve receber o commit de feature (feat).
Já o `README.md` é um arquivo que não influencia em nada no código e sim na documentação. Por isso deve ser um commit de docs.

### 5 - Copie e cole o código abaixo em sua calculadora.js:

```js
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const args = process.argv.slice(2);

soma();
```

Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança. 
É um commit refactor, já que o arquivo continua funcionando do mesmo jeito, mudando apenas a maneira como está estruturado.

### 6 - João entrou em seu repositório e o deixou da seguinte maneira:

```js
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
    console.log(parseInt(args[0]) - parseInt(args[1]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', arg[0]);
}
```
Depois disso, realizou um git add . 
e um commit com a mensagem: "Feature: added subtraction"
faça como ele e descubra como executar o seu novo código.

**RESPOSTA:**   
Ele não vai executar a princípio pois nos args estamos usando o arg da posição 0 tanto para passar a funcionalidade quanto para passar um número.

Nesse código, temos um pequeno erro, encontre-o e corrija 
para que a soma e divisão funcionem.

    **RESPOSTA:** Não tem divisão, mas para que soma e subtração funcione, o código deve ficar da seguinte maneira:

```js
const soma = () => {
    console.log(parseInt(args[1]) + parseInt(args[2]));
};

const sub = () => {
    console.log(parseInt(args[1]) - parseInt(args[2]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', args[0]);
}
```

Por fim, commit sua mudança. 

### 7 - Por causa de joãozinho, você foi obrigado a fazer correções na sua branch principal! O produto foi pro saco e a empresa perdeu muito dinheiro porque não conseguiu fazer as suas contas, graças a isso o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.

Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch. 

### 8 - Agora que a sua divisão está funcionando e você garantiu que não afetou as outras funções, você está apto a fazer um merge request. Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.

### 9 - João quis se redimir dos pecados e fez uma melhoria em seu código, mas ainda assim, continuou fazendo um push na master, faça a seguinte alteração no código e fez o commit com a mensagem: "refactor: calculator abstraction"

```js
var x = args[0];
var y = args[2];
var operator = args[1];

function evaluate(param1, param2, operator) {
  return eval(param1 + operator + param2);
}

if ( console.log( evaluate(x, y, operator) ) ) {}
```

Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, 
e seu chefe não descobriu que tudo está comprometido, faça um revert 
através do seu gitlab para que o produto volte ao normal o quanto antes!

### 10 - Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem. 

**RESPOSTA:** Precisamos primeiro salvar os args numa const com o `process.argv.slice(2)` e depois, no terminal, vamos passar os args separados por um operador (2 + 3) e a função irá retornar o resultado dessa operação.
A função eval converte uma string em código javascript. Logo, ele converte nossa expressão matemática passada nos args também para código, que é retornado para o usuário. 
